from python:3.9.16

COPY . .

RUN python -m pip install -r requirements.txt

ENV PYTHONPATH=src/todolist

EXPOSE 5000

CMD ["python", "-m", "flask", "--app", "src/todolist/app/todo", "run", "--host", "0.0.0.0"]
